set encoding=utf-8
set runtimepath+=~/.vim/bundle/Vundle.vim
call vundle#begin()

filetype off

" let Vundle manage Vundle
Plugin 'VundleVim/Vundle.vim'

" Import any bundles we need locally
" {{{ ~/.vimrc.bundle_local
if filereadable(resolve(expand('~/.vimrc.bundle_local')))
  source ~/.vimrc.bundle_local
endif
" }}}

Plugin 'tpope/vim-fugitive'

" YouCompleteMe isn't playing nicely with ale, disable it for now.
"Plugin 'valloric/YouCompleteMe'


Plugin 'w0rp/ale'

Plugin 'rust-lang/rust.vim'

Plugin 'fatih/vim-go'
Plugin 'cespare/vim-toml'
Plugin 'dart-lang/dart-vim-plugin'
Plugin 'airblade/vim-gitgutter'
Plugin 'powerline/powerline'
Plugin 'vim-scripts/bnf.vim'

call vundle#end()

filetype plugin indent on

let g:ale_set_quickfix = 1
let g:ale_set_loclist = 0
" Set the ale completion delay to 500ms
let g:ale_completion_delay = 500

let g:ale_completion_enabled = 0
let g:ale_c_clangtidy_checks = ['*', '-fuchsia-*']
let g:ale_cpp_clangtidy_checks = ['*', '-fuchsia-*']
let g:ale_linters = {'go': ['gofmt', 'golint', 'go vet', 'gometalinter', 'gopls'], 'rust': ['rls']}
let g:ale_go_gometalinter_options = '--fast'

let g:ycm_filetype_specific_completion_to_disable= {'gitcommit': 1, 'go': 1}

if has('cscope')
    set cscopeprg=/usr/bin/cscope
    set cscopetagorder=0
    set nocscopeverbose
    " add any database in current directory
    if filereadable('cscope.out')
        cs add cscope.out
        set cscopetag
    " else add database pointed to by environment
    elseif $CSCOPE_DB !=# ''
        cs add $CSCOPE_DB
        set cscopetag
    endif
    set cscopeverbose
endif

let g:go_highlight_functions = 1
let g:go_highlight_methods = 1
let g:go_highlight_structs = 1
let g:go_highlight_operators = 1
let g:go_highlight_build_constraints = 1
"let g:go_auto_sameids = 1
"let g:go_auto_type_info = 1
"let g:go_metalinter_autosave = 1

let g:go_test_show_name = 1
let g:go_gocode_propose_source = 1
let g:go_def_mode = 'gopls'

" Use gopls for go symbol renames
let g:go_rename_command = 'gopls'
let g:go_referrers_mode = 'gopls'
let g:go_gopls_options = ['--remote=auto']
"let g:ale_go_gopls_options = '--remote=auto'

" Add shortcuts for ALE reference finding and fixing.
nmap <leader>ar <Plug>(ale_find_references)
nmap <leader>af <Plug>(ale_fix)
nmap <leader>agd <Plug>(ale_go_to_definition)
nmap <leader>agt <Plug>(ale_go_to_type_definition)
nmap <leader>an <Plug>(ale_next_wrap)
nmap <leader>ap <Plug>(ale_previous_wrap)
nmap <C-]> <Plug>(ale_go_to_definition)

augroup gobindings
au FileType go nmap <leader>gi :GoImport 
au FileType go nmap <leader>i :GoImports<cr>
au FileType go nmap <leader>r :GoRename<cr>
au FileType go nmap <leader>t :GoInfo<cr>
au FileType go nmap <leader>ga :GoAlternate<cr>
au FileType go nmap <leader>gt :GoTest<cr>
au FileType go nmap <leader>gf :GoTestFunc<cr>
au FileType go nmap <leader>gr :GoReferrers<cr>
au FileType go nmap <leader>cr :GoCallers<cr>
au FileType go nmap <leader>ce :GoCallees<cr>
au FileType go nmap <leader>cp :GoChannelPeers<cr>
au FileType go nmap <leader>gs :GoFillStruct<cr>
"au FileType go nmap <C-]> :GoDef<cr>
augroup END

" Some rust settings
let g:rustfmt_autosave = 1

" make vimtex happy by telling we write LaTeX here:
let g:tex_flavor = 'latex'

" Set of filename patterns to override the filetypes on
augroup fnameoverride
autocmd BufRead,BufNew,BufNewFile,BufEnter BUILD.* setf bzl
autocmd BufRead,BufNew,BufNewFile,BufEnter APKBUILD setf sh
autocmd BufRead,BufNew,BufNewFile,BufEnter Jenkinsfile setf groovy
autocmd BufRead,BufNew,BufNewFile,BufEnter *.dockerfile setf dockerfile
augroup END

" Provide a visual marker when textwidth is set.
set colorcolumn=+1

" Save state between opening the same file.
set viminfo='100,<50,s10,r/tmp
" Only do this part when compiled with support for autocommands.
if has('autocmd')

  " Put these in an autocmd group, so that you can revert them with:
  " ":augroup vimStartup | au! | augroup END"
  augroup vimStartup
    au!

    " When editing a file, always jump to the last known cursor position.
    " Don't do it when the position is invalid or when inside an event handler
    " (happens when dropping a file on gvim).
    autocmd BufReadPost *
      \ if line("'\"") >= 1 && line("'\"") <= line("$") |
      \   exe "normal! g`\"" |
      \ endif

  augroup END

endif " has("autocmd")

" Highlight trailing whitespace

augroup trailingWhitespace
autocmd ColorScheme * highlight ExtraWhitespace ctermbg=darkgreen guibg=darkgreen
autocmd Syntax * syn match ExtraWhitespace /\s\+$\| \+\ze\t/ containedin=ALL
"autocmd Syntax * syn match ExtraWhitespace /[^\t]\zs\t\+/ containedin=ALL
augroup END

if exists('+termguicolors')
  " Override t_8f and t_8b to fix 24-bit color when run within tmux
  let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
  let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
  set termguicolors
  colo desert
  " override the background color to increase contrast
  highlight Normal ctermbg=black guibg=black
else
  colo desert256
endif


highlight GitGutterAdd ctermbg=242 guibg=Grey guifg=DarkGreen ctermfg=2
highlight GitGutterChange ctermbg=242 guibg=Grey guifg=DarkYellow ctermfg=3
highlight GitGutterDelete ctermbg=242 guibg=Grey guifg=DarkRed ctermfg=4

" {{{ ~/.vimrc.local
if filereadable(resolve(expand('~/.vimrc.local')))
  source ~/.vimrc.local
endif
" }}}
