# The following lines were added by compinstall

zstyle ':completion:*' completer _expand _complete _ignored _correct _approximate
zstyle ':completion:*' list-colors ''
zstyle ':completion:*' list-prompt %SAt %p: Hit TAB for more, or the character to insert%s
zstyle ':completion:*' matcher-list '' '' 'r:|[._-]=* r:|=*' 'l:|=* r:|=*'
zstyle ':completion:*' menu select=long
zstyle ':completion:*' completer _expand _complete _ignored _correct
zstyle ':completion:*' max-errors 2 numeric
zstyle ':completion:*' prompt 'found %e errors:'
zstyle :compinstall filename '~/.zshrc'

fpath=("/home/david/.zsh_comp/" ${fpath[@]})

autoload -Uz compinit
compinit
# End of lines added by compinstall
# Lines configured by zsh-newuser-install
HISTFILE=~/.zsh_histfile
HISTSIZE=100000
SAVEHIST=100000
# End of lines configured by zsh-newuser-install
#
#
setopt appendhistory extendedglob notify SHARE_HISTORY
bindkey -v

# Make ctrl-R search work properly.
bindkey "^R" history-incremental-search-backward

alias ls='ls --color=auto'

autoload -z edit-command-line
zle -N edit-command-line
bindkey -M vicmd v edit-command-line

# Include the current git branch/status in the prompt:
setopt prompt_subst
autoload -Uz vcs_info
zstyle ':vcs_info:*' check-for-changes true
zstyle ':vcs_info:*' check-for-staged-changes true
zstyle ':vcs_info:*' get-revision true
zstyle ':vcs_info:*' stagedstr '%F{green}S%f'
zstyle ':vcs_info:*' unstagedstr '%F{red}U%f'
zstyle ':vcs_info:*' actionformats \
    '%F{magenta}[%F{green}%b%F{yellow}|%F{red}%a%F{magenta}]%f%u '
zstyle ':vcs_info:*' formats       \
	'%F{magenta}[%F{green}%b%F{magenta}]%f%u '

zstyle ':vcs_info:*' enable git svn hg

precmd() {
    foo=$?
    time="$(date +'%F %T')"
    right="${time} (\$? = ${foo}) "
    print ${(l:$COLUMNS:: :)right}
    vcs_info
}
PS1=$'%B%(!.%F{red}.%F{green})%n@%m%f%b:%F{blue}%~%f ${vcs_info_msg_0_}%f%# '

if [ -f "$HOME/.zshrc.local" ]; then source "$HOME/.zshrc.local"; fi
